// chordnix - a bin and lib for chorded keyboard input
// Copyright (C) 2023  LoveIsGrief
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashSet;
use crate::mappers::base::Mapper;

pub mod mappers;

pub struct Chorder<'a>{
    pressed_keys: HashSet<&'a String>,
    mapper: &'a dyn Mapper
}

impl Chorder<'_> {
    pub fn new(mapper: &impl Mapper) -> Chorder{
        Chorder{
            pressed_keys: HashSet::new(),
            mapper,
        }
    }
}
