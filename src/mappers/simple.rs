// chordnix - a bin and lib for chorded keyboard input
// Copyright (C) 2023  LoveIsGrief
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
use std::collections::{HashMap, HashSet};

use crate::mappers::base::Mapper;

pub struct SimpleMapper {
    map: HashMap<String, String>,
}

impl SimpleMapper {
    fn new(map: HashMap<String, String>) -> SimpleMapper {
        SimpleMapper {
            map
        }
    }
}

impl Mapper for SimpleMapper {
    fn get(self: Self, pressed: HashSet<String>, previous: Vec<String>) -> Option<String> {
        let mut key_vec: Vec<String> = pressed.iter().map(|item| item.clone()).collect();
        key_vec.sort();
        let key = key_vec.join("");
        let result = self.map.get(&key);
        if let Some(value) = result {
            Some(value.clone())
        } else {
            None
        }
    }
}

#[cfg(test)]
mod test {
    use std::collections::{HashMap, HashSet};

    use crate::mappers::base::Mapper;
    use crate::mappers::simple::SimpleMapper;

    #[test]
    fn test_map() {
        let mapper = SimpleMapper::new(HashMap::from([
            ("aer".to_string(), "are".to_string()),
            ("ehrt".to_string(), "there".to_string()),
        ]));
        let pressed: HashSet<String> = HashSet::from_iter(
            "there".to_string().split("").into_iter().map(|item| item.to_string())
        );
        let string = "there".to_string();
        assert_eq!(mapper.get(pressed, vec![]), Some(string))
    }
}
