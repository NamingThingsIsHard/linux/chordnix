// chordnix - a bin and lib for chorded keyboard input
// Copyright (C) 2023  LoveIsGrief
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};
use std::ops::Deref;

use crate::mappers::base::Mapper;

struct WeightedCandidate {
    candidate: String,
    weight: f32,
}

impl PartialEq<Self> for WeightedCandidate {
    fn eq(&self, other: &Self) -> bool {
        self.weight == other.weight && self.candidate == other.candidate
    }
}

impl PartialOrd for WeightedCandidate {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.weight.partial_cmp(&other.weight)
    }
}

/// The word options available after a chord has been pressed
pub type Options = Vec<String>;

/// A mapping of a word to words that preceded it with their percentage thereof
///
/// For example this tree
///     alive
///     ├── are 50%
///     │   ├── they 80%
///     │   └── we 20%
///     ├── is 25%
///     │   ├── it 30%
///     │   └── that 70%
///     └── was 25%
///         └── it 100%
///  is stored as
///     alive
///     ├── are 50%
///     ├── is 25%
///     └── was 25%
///     are
///     ├── they 80%
///     └── we 20%
///     is
///     ├── they 30%
///     └── that 70%
///     was
///     └── it 100%
///
pub type LookBehindMap = HashMap<String, HashMap<String, f32>>;

/// Provides a method to calculate the weight of an word depending on how deep it is
///
/// How the position of an antecedent will impact its weight in a calculation
trait DepthWeighter {
    fn get_weight(&self, depth: u32, fraction: f32) -> f32;
}

/// An implementer of `DepthWeighter` where weight is directly correlated to the depth
/// --> Increasing depth has a higher impact on weight
/// This means the deeper, the higher the impact on the calculation
struct SimpleDepthWeight {}

impl DepthWeighter for SimpleDepthWeight {
    fn get_weight(&self, depth: u32, fraction: f32) -> f32 {
        depth as f32 * fraction
    }
}
/// An implementer of `DepthWeighter` where weight is inversely correlated to the depth
/// --> Increasing depth has lower impact on weight
/// This means the shallower, the higher the impact on the calculation
struct ReverseDepthWeighter {
    pub max_depth: u32,
}

impl DepthWeighter for ReverseDepthWeighter {
    fn get_weight(&self, depth: u32, fraction: f32) -> f32 {
        (self.max_depth - depth) as f32 * fraction
    }
}

/// An implementer of `DepthWeighter` where depth has a static impact on the weight calculation
struct StaticDepthWeighter {
    depth: u32
}

impl DepthWeighter for StaticDepthWeighter {
    fn get_weight(&self, depth: u32, fraction: f32) -> f32 {
        self.depth as f32 * fraction
    }
}

pub enum DepthWeighterType {
    Simple,
    Reverse,
    Static(u32),
}

/// A mapper which takes words that come before in consideration
///
/// It does so by verifying by building a path through a tree of words.
/// Each word has a list of antecedents with their corresponding occurrence in percentage.
pub struct LookBehindMapper {
    /// A mapping of chord to possible words
    /// Each chord must be a sorted string
    pub option_map: HashMap<String, Options>,
    /// The tree of antecedents and their occurrence percentage
    pub look_behind: LookBehindMap,
    /// Which DepthWeighter should be used when calculating the weight of an antecedent
    pub depth_weighter_type: DepthWeighterType,
}

impl Mapper for LookBehindMapper {
    fn get(self: Self, pressed: HashSet<String>, previous: Vec<String>) -> Option<String> {
        let mut key_vec: Vec<String> = pressed.iter().map(|item| item.clone()).collect();
        key_vec.sort();
        let key = key_vec.join("");
        let result = self.option_map.get(&key);
        if let Some(options) = result {
            let depth_weighter: Box<dyn DepthWeighter> = match self.depth_weighter_type {
                DepthWeighterType::Simple => { Box::new(SimpleDepthWeight {}) }
                DepthWeighterType::Reverse => { Box::new(ReverseDepthWeighter { max_depth: options.len() as u32 }) }
                DepthWeighterType::Static(depth) => { Box::new(StaticDepthWeighter { depth }) }
            };
            find_best_candidate(options, &self.look_behind, previous, &depth_weighter)
        } else {
            None
        }
    }
}

/// From the options, find the one that best fits the antecedents
///
/// # Arguments
///
/// * `options` - From which the best candidate should be selected
/// * `look_behind` -
/// * `antecedents` - Which words preceded the option to be selected
/// * `depth_weighter` - Object to calculate the weight of an antecedent
fn find_best_candidate(
    options: &Vec<String>,
    look_behind: &LookBehindMap,
    antecedents: Vec<String>,
    depth_weighter: &Box<dyn DepthWeighter>,
) -> Option<String> {
    let candidate_count = options.len();
    if candidate_count < 1 {
        None
    } else if candidate_count == 1 {
        options.first().cloned()
    } else {
        if antecedents.len() > 0 {
            let mut weighted_candidates: Vec<WeightedCandidate> = Vec::new();
            for candidate in options {
                if look_behind.contains_key(candidate) {
                    weighted_candidates.push(WeightedCandidate {
                        candidate: candidate.clone(),
                        weight: calc_weight(
                            &candidate,
                            0.0,
                            0,
                            0.0,
                            &antecedents[..],
                            look_behind,
                            &depth_weighter),
                    })
                }
            }
            weighted_candidates.sort_by(|left, right| left.weight.total_cmp(&right.weight));
            if let Some(greatest_candidate) = weighted_candidates.last() {
                Some(greatest_candidate.candidate.clone())
            } else {
                options.first().cloned()
            }
        } else {
            //     No history, but multiple options
            //     TODO: maybe track how many times an option was chosen and pick the most chosen one?
            options.first().cloned()
        }
    }
}

/// Recursive function to calculate the weight of a candidate using its depth in the look behind tree
///
/// # Arguments
///
/// * `candidate` - whose weight has to be calculated
/// * `percentage` - Occurrence rate of the candidate to the word it precedes
/// * `current_depth` - How deep in the tree the candidate is
/// * `current_weight` - How much the candidate currently weighs
/// * `path` - The remaining path in the tree that still (possibly) has to be traversed
/// * `look_behind` - General occurrence statistics
/// * `depth_weighter` - Object to calculate the weight of an antecedent
fn calc_weight(
    candidate: &String,
    percentage: f32,
    current_depth: u32,
    current_weight: f32,
    path: &[String],
    look_behind: &LookBehindMap,
    depth_weighter: &Box<dyn DepthWeighter>,
) -> f32 {
    let init_weight = depth_weighter.get_weight(current_depth, percentage) + current_weight;
    if let Some(word) = path.first() {
        if let Some(prev_map) = look_behind.get(candidate) {
            if let Some(child_percentage) = prev_map.get(word.as_str()).cloned() {
                let child_path = if path.len() > 1 {
                    &path[1..]
                } else {
                    &[]
                };
                calc_weight(word, child_percentage, current_depth + 1, init_weight, child_path, look_behind, &depth_weighter)
            } else {
                init_weight
            }
        } else {
            init_weight
        }
    } else {
        init_weight
    }
}

#[cfg(test)]
mod test {
    use std::collections::{HashMap, HashSet};

    use crate::mappers::base::Mapper;
    use crate::mappers::look_behind::{DepthWeighterType, LookBehindMapper};

    fn get_map() -> LookBehindMapper {
        LookBehindMapper {
            option_map: HashMap::from([
                ("ehrt".to_string(), vec!["there".to_string(), "three".to_string(), "ether".to_string()]),
            ]),
            look_behind: HashMap::from([
                ("there".to_string(), HashMap::from([
                    ("doing".to_string(), 0.1),
                    ("over".to_string(), 0.3),
                    ("going".to_string(), 0.4),
                    ("do".to_string(), 0.2),
                ])),
                ("ether".to_string(), HashMap::from([
                    ("doing".to_string(), 0.4),
                    ("over".to_string(), 0.3),
                    ("going".to_string(), 0.2),
                    ("do".to_string(), 0.2),
                ])),
            ]),
            depth_weighter_type: DepthWeighterType::Simple,
        }
    }

    #[test]
    fn test_map() {
        let mapper = get_map();
        let pressed: HashSet<String> = HashSet::from_iter(
            "there".to_string().split("").into_iter().map(|item| item.to_string())
        );
        assert_eq!(mapper.get(pressed, vec!["going".to_string()]), Some("there".to_string()));
    }

    #[test]
    fn test_ether() {
        let mapper = get_map();
        let pressed: HashSet<String> = HashSet::from_iter(
            "there".to_string().split("").into_iter().map(|item| item.to_string())
        );
        assert_eq!(mapper.get(pressed, vec!["doing".to_string()]), Some("ether".to_string()));
    }
}
